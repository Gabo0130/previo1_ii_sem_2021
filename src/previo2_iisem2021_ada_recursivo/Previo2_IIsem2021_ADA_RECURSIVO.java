/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package previo2_iisem2021_ada_recursivo;

/**
 *
 * @author Jhony gabriel quintero gomez
 */
public class Previo2_IIsem2021_ADA_RECURSIVO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Vector de ejemplo:
        int v[] = {3, 4, 5, 6};
        //LLamado de su método:
        String msg = "";
        System.out.print(imprimir(v, v.length-1, msg));
    }

    /**
     * Escriba acá su método recursivo , recuerde colocarle el modificador
     * static
     */
    private static String imprimir(int v[], int i, String msg) {
        if (i == -1) {
            return msg;
        } else {
            msg=msg+"[";
               for(int j=i;j>-1;j--){
                   if(j!=0)
                      msg=msg+v[j]+",";
                   else
                       msg=msg+v[j];
               }
               msg=msg+"]\n";
               return imprimir(v,--i,msg);
        }
}
}
